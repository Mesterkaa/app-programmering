import { Routes } from "./Routes";

import { SubscriptionController } from "../controllers/subscriptionController";

/**
 * Subroutes for the subscription API.
 */
export class SubscriptionRoutes extends Routes{
   private subscriptionController: SubscriptionController = new SubscriptionController();

   constructor(){
       super();
       this.routes();
   }
   protected routes() {
    // Because there only are one, it uses just uses the main route.
    this.router.post("", this.subscriptionController.subscribe);
    }
}
