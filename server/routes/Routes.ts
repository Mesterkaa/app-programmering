import { Router } from "express";

/**
 * Base route class
 */
export abstract class Routes {
    public router: Router;

    constructor() {
        this.router = Router();

    }

    protected abstract routes(): void
}
