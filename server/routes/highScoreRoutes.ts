import { Routes } from "./Routes";

import { HighScoreController } from "../controllers/highScoreController";

/**
 * Subroutes for the highscore API.
 */
export class HighScoreRoutes extends Routes{
   private highScoreController: HighScoreController = new HighScoreController();

   constructor(){
       super();
       this.routes();
   }
   protected routes() {
    // Two routes are defined.
    // One for getting scores, and one for saving scores.
    this.router.get("/scores/:uuid", this.highScoreController.getScores);
    this.router.put("/savescores", this.highScoreController.saveScores);
    }
}
