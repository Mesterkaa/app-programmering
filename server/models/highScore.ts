import { Document, Schema, Model, model, Error } from "mongoose";
import { Client, PlaceType2, ReverseGeocodeRequest } from "@googlemaps/google-maps-services-js";
import { Record } from "./record";
import { SubscriptionService } from "../services/subscriptionService";
import { GOOGLE_API } from "server/config/secrets";

//Interface, Schema and Model for a Highscore

export interface IHighScore extends Document {
  name: string; //The name of user that made the score
  id: number; //This id is not mongodb id (SID), but the local indexeddb
  device_id: string; //Uuid of the device that is was made on.
  score: number; //Score
  latitude?: number; //Latitude
  longitude?: number; //Longitude
  location?: string; //Location name found by Google based on latitude and longitude
}

export const highScoreSchema: Schema = new Schema({
  name: {type: String, required: true, trim: true},
  id: {type: Number, required: true},
  device_id: {type: String, required: true},
  score: {type: Number, required: true},
  latitude: {type: Number, required: false},
  longitude: {type: Number, required: false},
  location: {type: String, required: false}
});

/**
 * Before a highscore is saved finally saved to the database, this code is set to run
 */
highScoreSchema.pre<IHighScore>("save", async function save(next) {
  const newScore = this;

  //Sets up the Google API service
  const client = new Client();

  //If a API key is stored
  if (GOOGLE_API) {
    //Finds Geocode information based on longitude and latitude
    let Geocode = await client.reverseGeocode({ params: {latlng: `${this.latitude},${this.longitude}`, result_type: [PlaceType2.locality], key: GOOGLE_API}});
    //And stores a location name like "Hasselager", "Viby J" to the almost saved highscore.
    newScore.location = Geocode.data.results[0].address_components[0].short_name;
  }
  //It when tries to find stored Record with a device_id that match.
  let record = await Record.findOne({device_id: newScore.device_id}).populate('score').exec();

  if (record != null) {
    //If a Record is found, if the new score is higher update the record
    let oldscore = (record.score as IHighScore).score;
    if (oldscore < newScore.score) {
      record.score = newScore._id;
      await record.save();
    }
  } else {
    //If no record was found create one.
    await Record.create({device_id: newScore.device_id, score: newScore._id});
  }
  //It when tries to notify any beat records, with this service.
  let subscriptionService: SubscriptionService = new SubscriptionService();
  subscriptionService.notify(this.score);
  //next(); completes the saving.
  next();
});

export const HighScore: Model<IHighScore> = model<IHighScore>("HighScore", highScoreSchema);
