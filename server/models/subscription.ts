import { Document, Schema, Model, model, Error } from "mongoose";

//Interface, Schema and Model for a subscription.

export interface ISub extends Document {
  device_id: string;
  subscription: string;
}

export const subSchema: Schema = new Schema({
  device_id: {type: String, required: true},
  subscription: {type: String, required: true}
});

export const Sub: Model<ISub> = model<ISub>("Sub", subSchema);
