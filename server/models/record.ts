import { Document, Schema, Model, model, Error } from "mongoose";
import { IHighScore } from "./highScore";

//Interface, Schema and Model for a Record
//Scores is a foreign key to HighScore

export interface IRecord extends Document {
  score: IHighScore['_id'];
  device_id: string;
}

export const recordSchema: Schema = new Schema({
  score: {
    type: Schema.Types.ObjectId,
    ref: 'HighScore'
  },
  device_id: {type: String, required: true}
});

export const Record: Model<IRecord> = model<IRecord>("Record", recordSchema);
