import * as express from 'express';
import * as http from 'http';
import * as cors from 'cors';

import * as mongoose from 'mongoose';

import { MONGODB_URI } from "./config/secrets";
import { HighScoreRoutes } from './routes/highScoreRoutes';
import { SubscriptionRoutes } from './routes/subscriptionRoutes';


/**
 * My server object. It controls everything.
 */
class Server {

  public app: express.Application;

  constructor() {
    this.app = express();
    this.config();
    this.mongo();
    this.routes();
  }

  /**
   * Setting up the routes the express server is responding to.
   */
  private routes(): void{
    //Have two main routes. A route for scores and a route for subscriptions. Both of these have subroutes.
    this.app.use("/score", new HighScoreRoutes().router);
    this.app.use("/subscription", new SubscriptionRoutes().router);
  }
  /**
   * Basic settings of the express app. Settings other than routing is done here.
   */
  private config(): void {
    this.app.use(cors());
    this.app.use(express.json());
    this.app.use(express.urlencoded({extended: false}));
  }
  /**
   * Connectes to a Mongodb with mongoose.
   * It will try to reconnect on a disconnect.
   */
  private mongo(): void {
    //First it sets up the configuration.
    const connection = mongoose.connection;
    connection.on("connected", () => {
      console.info("Mongo Connection Established");
    });
    connection.on("reconnected", () => {
      console.info("Mongo Connection Reestablished");
    });
    connection.on("disconnected", () => {
      console.warn("Mongo Connection Disconnected");
      console.warn("Trying to reconnect to Mongo ...");
      setTimeout(()=> {
        mongoose.connect((MONGODB_URI as string), {
          keepAlive: true,
          socketTimeoutMS: 3000, connectTimeoutMS: 3000,
          useNewUrlParser: true, useUnifiedTopology: true
        } as mongoose.ConnectOptions);
      }, 3000)
    });
    connection.on("close", () => {
      console.info("Mongo Connection Closed");
    });
    connection.on("error", (error: Error) => {
      console.error("Mongo Connection ERROR: " + error)
    });
    //When it tries to connect. The connection string/URI is stored in an enviroment file, so not to get pushed to git
    const run = async () => {

      await mongoose.connect((MONGODB_URI as string), {
        keepAlive: true, useNewUrlParser: true, useUnifiedTopology: true
      } as mongoose.ConnectOptions);
    };
    run().catch(error => console.error(error));
  }
  /**
   * Starting the server. Port 5000 is used so i could portforward to my mobile for testing.
   */
  public start(): void {
    const httpServer = http.createServer(this.app);

    httpServer.listen(5000, () => {
      console.log('info', 'HTTP Server running on port 5000');
    });
  }
}

const server = new Server();
server.start();
