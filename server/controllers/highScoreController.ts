import { NextFunction, Request, Response } from "express";
import { HighScoreService } from "../services/highScoreService";

export class HighScoreController{
    private highScoreService: HighScoreService = new HighScoreService();

    constructor() {
        this.getScores = this.getScores.bind(this);
        this.saveScores = this.saveScores.bind(this);
    }

    /**
     * Getting the score for the client.
     * Responding with the scores.
     * @param req Request
     * @param res Response
     * @param next NextFunction
     */
    public async getScores(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
          //Getting the uuid from the request and using it with the service.
          let uuid = req.params.uuid;
          const scores = await this.highScoreService.getScores(uuid);
          //Responeds with the scores
          res.send(scores);
        } catch (error) {
          //if an error happens next is called to handle it.
          next(error);
        }
    }

    /**
     * Saving scores from the client.
     * Responding the saved score/scores.
     * @param req Request
     * @param res Response
     * @param next NextFunction
     */
    public async saveScores(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
          //The service is called with scores from the body.
          const scores = await this.highScoreService.saveScores(req.body.scores);
          res.send(scores);
        } catch (error) {
          //if an error happens next is called to handle it.
            next(error);
        }
    }
}
