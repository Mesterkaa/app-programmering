import { NextFunction, Request, Response } from "express";
import { SubscriptionService } from "../services/subscriptionService";

export class SubscriptionController{
  private subscriptionService: SubscriptionService = new SubscriptionService();

    constructor() {
        this.subscribe = this.subscribe.bind(this);
    }

    /**
     * Saving a subscription object.
     * @param req Request
     * @param res Response
     * @param next NextFunction
     */
    public async subscribe(req: Request, res: Response, next: NextFunction): Promise<void> {
        try {
          //Using the service to store the PushSubscription object.
          await this.subscriptionService.subscribe(req.body.sub);
          res.send("Done");
        } catch (error) {
          //if an error happens next is called to handle it.
          next(error);
        }
    }
}
