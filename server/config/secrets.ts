
import * as dotenv from "dotenv";
dotenv.config();


//Loads the Mongodb connection string from the enviroment. I usally store it in a .env file.
export const MONGODB_URI = process.env["MONGODB_URI"];

if (!MONGODB_URI) {
  console.error("No mongo connection string. Set MONGODB_URI environment variable.");
  process.exit(1);
}

//Loads the GOOGE_API Key from the enviroment. I usally store it in a .env file.
export const GOOGLE_API = process.env["GOOGLE_API"];

if (!GOOGLE_API) {
  console.error("No Google API key was supplied Set GOOGLE_API environment variable.");
  process.exit(1);
}
