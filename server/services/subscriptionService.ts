import { ISub, Sub } from "../models/subscription";
import { IHighScore } from "../models/highScore";
import { PushSubscription, sendNotification, setVapidDetails } from "web-push";
import { Record } from "../models/record";

const vapidKeys = {
  "publicKey":"BOsKJH7PGBKoVQTScjeSDOaKo0vV6mOjQyM7Jw4JWY65-y21vI6LxFh6xjoNebw_l0rxlKjJ5-tjnbKFLcaLvQ0",
  "privateKey":"rpkVp4NH5p_KD9fJ6V7oS5Ny9iiSXw-Mn_HHc58SgII"
};



export class SubscriptionService {

    /**
     * Saves a subscription
     * @param sub PushSubscription object
     * @returns The saves object
     */
    public async subscribe(sub: ISub): Promise<ISub | null> {
      return await Sub.create(sub);
    }

    /**
     * Notifies beat users, that there highscore records was beat.
     * @param newscore The new higher score.
     */
    public async notify(newscore: number): Promise<void> {
      //Finds all the records and joins the score by the score id.
      let allRecords = await Record.find({}).populate('score').exec();
      //Filtes the array to only contains records that was beat
      let beatRecords = allRecords.filter(e => {
        if (e.score != null) {
          return (e.score as IHighScore).score < newscore
        } else {
          return false
        }

      });
      //Creates a map only with the device_id's
      let device_ids = beatRecords.map(e => {return e.device_id});
      //When goes though subcripters and find matches based on that list, and maps to and array only containing the PushSubscription object.
      let beatSubs: PushSubscription[] = (await Sub.find({device_id: { "$in": device_ids }})).map(e => {return JSON.parse(e.subscription)});

      //Defining the Payload
      const notificationPayload = {
        "notification": {
            "title": "Highscore beat",
            "body": "Your highscore was beat by another user",
            "vibrate": [100, 50, 100],
            "data": {
                "dateOfArrival": Date.now(),
                "primaryKey": 1
            },
            "actions": [{
                "action": "explore",
                "title": "Try to beat it"
            }]
        }
      };
      setVapidDetails(
        'mailto:awkaa@outlook.dk',
        vapidKeys.publicKey,
        vapidKeys.privateKey
      );

      //Creates an options object.
      let options = {
        vapidDetails: {
        subject: 'mailto:awkaa@outlook.dk',
        publicKey: vapidKeys.publicKey,
        privateKey: vapidKeys.privateKey
        }
      }
      //One by one starts the async function to send a Notifaction to the beat users.
      Promise.all(beatSubs.map(sub => sendNotification(
        sub, JSON.stringify(notificationPayload), options )))
        .then(() => { console.log("Succes sending notification")})
        .catch(err => {
            console.error("Error sending notification, reason: ", err);
        });
    }
}
