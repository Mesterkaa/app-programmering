import { IHighScore, HighScore } from "../models/highScore";

export class HighScoreService {

  /**
   * Get the top 10 scores.
   * @param uuid Uniuqe id generated by the client.
   * @returns Highscores that didn't come from a client with that uuid.
   */
  public async getScores(uuid: string): Promise<IHighScore[] | null> {
    //Finds highscore, which device_id doesn't equal  the uuid.
    //Sorting it for highest scores first, and taking top 10.
    //It means it gets the 10 highest scores that does not comes from the client.
    return await HighScore.find({ "device_id": { "$ne": uuid } }).sort({score: -1}).limit(10);
  }

  /**
   * Saves incoming scores
   * @param data Array of highscores to be saved.
   * @returns The saved highscores with id's and location names.
   */
  public async saveScores(data: IHighScore[]): Promise<IHighScore[] | null> {
    //Bulk saves the incoming highscores.
    return await HighScore.create(data);
  }
}
