import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HighscoreComponent } from './pages/highscore/highscore.component';
import { MainComponent } from './pages/main/main.component';

//Here i defines the routes for the application. I could also add default, errors and more.
const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'highscore', component: HighscoreComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
