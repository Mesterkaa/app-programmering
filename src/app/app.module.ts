import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatMenuModule} from '@angular/material/menu';
import {MatListModule} from '@angular/material/list';
import {MatButtonModule} from '@angular/material/button';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';
import { FormsModule } from '@angular/forms';

import { DBConfig, NgxIndexedDBModule } from 'ngx-indexed-db';
import { HttpClientModule } from '@angular/common/http';
import {DragDropModule} from '@angular/cdk/drag-drop';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainComponent } from './pages/main/main.component';
import { HighscoreComponent } from './pages/highscore/highscore.component';
import { indexedDBConst } from './helper/contants.helper';
import { DraganddropComponent } from './pages/main/draganddrop/draganddrop.component';

const dbConfig: DBConfig  = {
  name: 'ClickGame',
  version: 1,
  objectStoresMeta: [{
    store: indexedDBConst.HIGHSCORE,
    storeConfig: { keyPath: 'id', autoIncrement: true },
    storeSchema: [
      { name: '_id', keypath: '_id', options: { unique: true } },
      { name: 'device_id', keypath: 'device_id', options: { unique: false } },
      { name: 'name', keypath: 'name', options: { unique: false } },
      { name: 'score', keypath: 'score', options: { unique: false } },
      { name: 'latitude', keypath: 'latitude', options: {unique: false }},
      { name: 'longitude', keypath: 'longitude', options: {unique: false }},
      { name: 'location', keypath: 'location', options: {unique: false }},
    ]
  }, {
    store: indexedDBConst.PLAYER,
    storeConfig: { keyPath: 'id', autoIncrement: true },
    storeSchema: [
      { name: 'name', keypath: 'name', options: { unique: false } },
      { name: 'uuid', keypath: 'uuid', options: { unique: true } }
    ]
  }, {
    store: indexedDBConst.OFFLINESCORES,
    storeConfig: { keyPath: 'id', autoIncrement: true },
    storeSchema: [
      { name: 'score_id', keypath: 'scoreId', options: { unique: true } }
    ]
  }  ]
};

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    HighscoreComponent,
    DraganddropComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
    NgxIndexedDBModule.forRoot(dbConfig),
    HttpClientModule,
    BrowserAnimationsModule,
    DragDropModule,

    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    MatToolbarModule,
    MatMenuModule,
    MatListModule,
    MatButtonModule,
    MatGridListModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
