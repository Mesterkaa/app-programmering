/**
 * Start a device vibration.
 * @param time The time to vibrate in ms or a vibrate pattern. Set to 0, to cancel running vibrations.
 */
export async function vibrate(time: number | number[]): Promise<void> {
  //First check if vibrate is accessible
  if (navigator.vibrate) {
    //Calls the Vibrate API to vibrate the device for set amount.
    navigator.vibrate(time);
  }

}


