/**
 * A collection of constant used when selecting wich table to get data from, in indexedDB
 * This may i don't by mistake, spell it wrong
 */
export enum indexedDBConst {
  HIGHSCORE = "highscore",
  PLAYER = "player",
  OFFLINESCORES = "offscore"
}
