
/**This code simple displays a particle at the location specified.
 *
 * @param x X coordinate of the starting place
 * @param y Y coordinate of the starting place
 */
export async function createParticle(x: number, y: number): Promise<void> {
  // Create a custom particle element
  let particle = document.createElement('particle');
  //And add the particle class to it. The styles for this class is in styles.scss
  particle.classList.add('particle');
  // Append the element into the body
  document.body.appendChild(particle);

  //I determine the size, randomly
  const size = Math.floor(Math.random() * 20 + 5);
  // Apply the size on each particle
  particle.style.width = `${size}px`;
  particle.style.height = `${size}px`;
  // Generate a random color
  particle.style.background = `rgb(${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)})`;

    // Generate a random x & y destination within a distance of 75px from the starting position
  const destinationX = x + (Math.random() - 0.5) * 2 * 75;
  const destinationY = y + (Math.random() - 0.5) * 2 * 75;

  // Store the animation in a variable because we will need it later
  const animation = particle.animate([
    {
      // Set the origin position of the particle
      // We offset the particle with half its size to center it around the mouse
      transform: `translate(${x - (size / 2)}px, ${y - (size / 2)}px)`,
      opacity: 1
    },
    {
      // We define the final coordinates as the second keyframe
      transform: `translate(${destinationX}px, ${destinationY}px)`,
      opacity: 0
    }
  ], {
    // Set a random duration from 500 to 1500ms
    duration: 500 + Math.random() * 1000,
    easing: 'cubic-bezier(0, .9, .57, 1)',
    // Delay every particle with a random value from 0ms to 200ms
    delay: Math.random() * 200
  });
  animation.onfinish = () => {
    particle.remove();
  };
}
