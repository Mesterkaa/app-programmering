import { Injectable } from '@angular/core';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { BehaviorSubject, Observable } from 'rxjs';
import { indexedDBConst } from '../helper/contants.helper';
import { highscore as highscore } from '../models/highscore.model';
//import { GeoLocationService } from './geolocation.service';
import { HttpClient } from '@angular/common/http';
import { offlinescore } from '../models/offlinescore.model';
import { PlayerService } from './player.service';

/**
 * A service to get the current highscores. Subscribe to 'highscore' to get the current highscore.
 */
@Injectable({
  providedIn: 'root'
})
export class HighscoreService {

  /**
   * The highscores is saved in the _list variable.
   * When a page wants to get a highscore it subscribes to the highscore observable.
   * When some code first subscribes it gets the latest message.
   */
  private _list: highscore[] = [];
  private _highscore: BehaviorSubject<highscore[]> = new BehaviorSubject<highscore[]>([]);
  public readonly highscore: Observable<highscore[]> = this._highscore.asObservable();

  /**
   * On startup the highscore services, gets the all the scores from indexedDB and pushes it to the subscribers.
   * indexedDB is a local running db.
   */
  constructor(private dbService: NgxIndexedDBService, private http: HttpClient, public playerService: PlayerService) {
    this.refreshHighscore();
  }

  /**
   * Refreshes the highscore list to sync with the online scores.
   */
  private async refreshHighscore(): Promise<void> {
    //.getAll simply gets all the records from the table.
    this.dbService.getAll<highscore>(indexedDBConst.HIGHSCORE).subscribe(async (highscore) => {
      this._list = highscore;
      //If the device is online, other devices scores from the servers database is merged with the local scores.
      if (navigator.onLine) {
        let i = await this.http.get<highscore[]>("http://localhost:5000/score/scores/" + this.playerService.uuid()).toPromise();
        this._list = this._list.concat(i);
      }
      //No matter if the device was online or not, the list is pushed to the html.
      this._highscore.next(this._list);
    })
  }

  /**
   * Add a new score to the highscore list.
   * The functions only keeps the top ten, removes the extras, pushes it to the subscribers and saves it to indexedDB
   *
   * @param score The new score to add to the highscore.
   */
  public addScore(score: highscore): void {
    //.addItem adds a single item to the table. When it's done it returns the new item, with the new key.
    this.dbService.addItem<highscore>(indexedDBConst.HIGHSCORE, score).subscribe(async (item) => {

      //First it check if the device is online, so it can talk to the server.
      if (navigator.onLine) {
        //It gets the id of the scores what was made previusly but wasn't save online, because it was offline.
        let offscore = await this.dbService.getAll<offlinescore>(indexedDBConst.OFFLINESCORES).toPromise();

        //The next 8 lines. Gets the result of offlines scores and creates an array of just the id to the actuale scores.
        //It then, one by one, get the full score object, and pushed them together with the new score.
        let keys = offscore.map(element => {return element.score_id});
        let promise: Promise<void>[] = [];
        let items: highscore[] = [item];
        keys.forEach(key => {
          promise.push(this.dbService.getByKey<highscore>(indexedDBConst.HIGHSCORE, key).toPromise().then(i => {
            items.push(i);
          }));
        })
        //awaiting on the dbService calls to finish.
        await Promise.allSettled(promise);;

        //All the scores is then sent to the server to be saved.
        this.http.put<highscore[]>("http://localhost:5000/score/savescores", {scores: items}).subscribe(highscores => {
          highscores.forEach(highscore => {
            //After they have been saved on the server they are return. Now the score has an _id and a location name. This info is saved to the local db.
            this.dbService.update(indexedDBConst.HIGHSCORE, highscore).toPromise().finally();
            //Because the offline scores is now saved online, the offline score db is cleared.
            this.dbService.clear(indexedDBConst.OFFLINESCORES).toPromise().finally();
          })
        }, e => {
          console.error(e);
        }, () => {
          //Finally, even after an error, it refreshes the highscore.
          this.refreshHighscore();
        });
      } else {
        //If the device was offline, the id of the scores is added to another local db.
        //This is used the next time the device is online.
        if (item.id) this.dbService.addItem<offlinescore>(indexedDBConst.OFFLINESCORES, {score_id: item.id}).toPromise().finally();
        this.refreshHighscore();
      }
    });
  }

  /**
   * Clear the highscore list. The new empty list is then push to the subscribers
   */
  public clear(): void {
    this._list = [];
    //.clear deletes all records in the database. When it's done, it pushes the empty list to the subscribers
    this.dbService.clear(indexedDBConst.HIGHSCORE).subscribe((successDeleted) => {
      this._highscore.next(this._list);
    });

  }






}
