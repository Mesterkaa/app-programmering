import { Injectable } from '@angular/core';
import { NgxIndexedDBService } from 'ngx-indexed-db';
import { BehaviorSubject, Observable } from 'rxjs';
import { indexedDBConst } from '../helper/contants.helper';
import { player } from '../models/player.model';
import { v4 as uuidv4 } from 'uuid';
import { SwPush } from '@angular/service-worker';
import { HttpClient } from '@angular/common/http';
/**
 * A service to get the current player name. Subscribe to 'name' to get the current name.
 */
@Injectable({
  providedIn: 'root'
})
export class PlayerService {

  private readonly VAPID_PUBLIC_KEY = "BOsKJH7PGBKoVQTScjeSDOaKo0vV6mOjQyM7Jw4JWY65-y21vI6LxFh6xjoNebw_l0rxlKjJ5-tjnbKFLcaLvQ0";
  private player: player | undefined;
  private _name: BehaviorSubject<string> = new BehaviorSubject<string>('');
  public readonly name: Observable<string> = this._name.asObservable();

  //The uuid is a randomly generated value, thats uniuqe to this device. The uuid is random, generated locally, but practally uniuqe.
  private _uuid: string = '';
  public uuid(): string {
    return this._uuid;
  }

  constructor(private dbService: NgxIndexedDBService, private swPush: SwPush, private http: HttpClient,) {

    //.getAll() gets all the player names stored, but there should only be one saved.
    this.dbService.getAll<player>(indexedDBConst.PLAYER).subscribe(async (player: player[]) => {
      //if no name was saved, the name '' is pushed to the subscribers, otherwise it pushes the saved name.
      //Same goes if for uuid, a new is generated if nothing was stored.
      if (player.length == 0) {
        this._name.next('');
        this._uuid = uuidv4();
      } else {
        this._name.next(player[0].name);
        this._uuid = player[0].uuid;
        this.player = player[0];
      }
    })
  }

  /**
   * Creates a subscription to get PUSH notifications from the save. Generates identifiers to be used by the server.
   */
  private createSub(): void {
    //Using service Works Push to generate the identifers. With the servers public_key
    this.swPush.requestSubscription({
      serverPublicKey: this.VAPID_PUBLIC_KEY
    }).then((sub: PushSubscription) => {
      //The PushSubscription object is then sent to the server for storage.
      this.http.post<any>("http://localhost:5000/subscription", {sub: {device_id: this._uuid, subscription:  JSON.stringify(sub)}}).subscribe(e => {
        console.log("Subscription done");
      });
    }).catch(err => console.error("Could not subscribe to notifications", err));
  }

  /**
   * Sets the player name to the provided name
   * @param name The new player name
   */
  public setName(name: string): void {
    if (this.player) {
      //If a player had been saved previosly, it updates the record with the new name.
      this.dbService.update<player>(indexedDBConst.PLAYER, this.player).subscribe((player: player[]) => {
        //The new name is saved and pushed to subscribers
        if (this.player) this.player.name = name;
        this._name.next(player[0].name);
      })
    } else {
      //If no player has been saved before, a new record is added.
      this.dbService.addItem<player>(indexedDBConst.PLAYER, {name: name, uuid: this.uuid()}).subscribe((player: player) => {
        //The create sub is called here, because in some browsers the user needs to interract the the page, before it can ask for permissions.
        this.createSub();
        this.player = player;
        this._name.next(this.player.name);
      })
    }
  }
}
