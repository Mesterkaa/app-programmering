import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
/**
 * GeoLocationService is only used to get
 */
export class GeoLocationService {

  constructor(private http: HttpClient) {
  }
  /**
 * A function to get GPS coordinates of the device.
 * @param success A callback function that is called on succes, with a geolocation parameter
 * @param error A callback function that is called on an error, with an error parametet
 */
public getGPSLocation(success: (geo: GeolocationPosition) => void, error: (error: GeolocationPositionError) => void): void {
  //It first check if geolocation is accessible
  if (navigator.geolocation) {
    //I get a geoLocation instance and call getCurrentPosition.
    //If it's the first time using geolocation, it asks the user permision to use it.
    //An error is also called if the user says no?
    let geo = navigator.geolocation;
    geo.getCurrentPosition(success, error, {
      enableHighAccuracy: true
    })
  }
}

/**
 * A simple function that converts degress to Radians
 * @param degrees
 * @returns Radians
 */
private degressToRadians(degrees: number): number {
  return degrees * Math.PI / 180;
}

/**
 * Calculates in the distance between to GPS coords using Haversine formula.
 * @param lat1 Latitude for coordinate 1.
 * @param lon1 Longitude for coordinate 1.
 * @param lat2 Latitude for coordinate 2.
 * @param lon2 Latitude for coordinate 2.
 * @returns A distance in kilometers
 */
public distanceBetween(lat1: number | undefined, lon1: number | undefined, lat2: number | undefined, lon2: number | undefined): number{
  if (lat1 && lon1 && lat2 && lon2) {
    let dLat = this.degressToRadians(lat2 - lat1);
    let dLon = this.degressToRadians(lon2 - lon1)

    lat1 = this.degressToRadians(lat1);
    lat2 = this.degressToRadians(lat2);

    var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    return 6371 * c;
  } else {
    return 0;
  }

}

/**
 * The distance distanceBetween() returns has many digits, so this formats it for use in the HTML.
 * When under 0.1 it just returns '< 0.1'
 * @param distance
 * @returns Clean up string of the distance
 */
public distanceFormat(distance: number): string {
  if (distance < 0.1) {
    return '< 0.1';
  } else {
    return distance.toFixed(1).toString();
  }
}
}
