/**
 * A simple interface of a highscore
 */
export interface highscore {
  id?: number
  _id?: string
  device_id?: string
  name: string
  score: number
  latitude?: number
  longitude?: number
  location?: string
}
