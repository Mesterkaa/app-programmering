/**
 * A simple interface of a player
 */
 export interface player {
  id?: number
  name: string
  uuid: string
}
