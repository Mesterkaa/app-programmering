/**
 * interface for an offline score
 */
 export interface offlinescore {
  id?: number
  score_id: number
}
