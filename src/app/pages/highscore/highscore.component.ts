import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { highscore } from 'src/app/models/highscore.model';
import { HighscoreService } from 'src/app/services/highscore.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { GeoLocationService } from 'src/app/services/geolocation.service';
import { PlayerService } from 'src/app/services/player.service';

@Component({
  selector: 'app-highscore',
  templateUrl: './highscore.component.html',
  styleUrls: ['./highscore.component.scss']
})
/**
 * Highscore page.
 */
export class HighscoreComponent implements OnInit, OnDestroy {

  private subscription: Subscription = Subscription.EMPTY;
  public uuid: string = this.playerService.uuid();
  //If the uuid of the row matches the uuid of this player, the colour of the row changes in the html.
  public matchUuid(device_id: string): boolean { return device_id === this.playerService.uuid();}
  public latitude: number = 0;
  public longitude: number = 0;
  private watchid: number = -1;

  public displayedColumns: string[] = ['name', 'score', 'location', 'distance'];
  public datasource!: MatTableDataSource<any>;

  @ViewChild(MatSort) private sort!: MatSort;
  @ViewChild(MatPaginator) private paginator!: MatPaginator;


  constructor(public highscoreService: HighscoreService, public geoLocationService: GeoLocationService, public playerService: PlayerService) { }

  public ngOnInit(): void {
    //By subscribing to the highscore, every time an update is made, this code is triggered.
    this.subscription = this.highscoreService.highscore.subscribe(highscores => {
      //The dataSource is set to a type MatTableDataSource. The type is used by the table to interact with the data.
      this.datasource = new MatTableDataSource(highscores);
      setTimeout(() => {
        //sort and paginator is added to the datasource. The two variables is added wiht a @ViewChild decorater so the table has access to the sorting and paginator component
        this.datasource.sort = this.sort;
        this.datasource.paginator = this.paginator;
        //Because i in the HTML calculate the distance, the table doesn't know how to sort it.
        //I added the sortingDataAccessor so i could specify what data i should sort with, when it sorts distance.
        this.datasource.sortingDataAccessor = (item, property) => {
          if (property === 'distance') {
            return this.geoLocationService.distanceBetween(item.latitude, item.longitude, this.latitude, this.longitude);
          } else {
            return item[property];
          }
        }
        });
    })
    //I set a callback function to when the GPS position changes. Like adding a eventlistener.
    this.watchid = navigator.geolocation.watchPosition((position) => {
      this.latitude = position.coords.latitude;
      this.longitude = position.coords.longitude;
    });
  }

  public ngOnDestroy(): void {
    //When this component is destroy (By going to another page), i make sure to unsubscribe, and clear watch, to make sure i don't run unintended code.
    this.subscription.unsubscribe();
    if (this.watchid != -1) {
      navigator.geolocation.clearWatch(this.watchid);
    }
  }

  /**
   * When triggered it clear the highscore by calling the highscoreService.
   */
  public clearHighscore(): void {
    //If the user clicks ok, in the dialog window confirm() returns true. confirm comes from the basic window scope.
    if(confirm("Are you sure you want to clear the highscore?")) {
      this.highscoreService.clear();
    }
  }

}
