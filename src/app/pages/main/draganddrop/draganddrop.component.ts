import { CdkDragEnd, CdkDragMove, CdkDragStart } from '@angular/cdk/drag-drop';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { createParticle } from 'src/app/helper/particles.helper';

@Component({
  selector: 'app-draganddrop',
  templateUrl: './draganddrop.component.html',
  styleUrls: ['./draganddrop.component.scss']
})
/**
 * Drag and drop component, is used on the frontpage. The logic is encapsulated here, to not clutter the main component.
 */
export class DraganddropComponent implements OnDestroy {

  private x: number = -1;
  private y: number = -1;
  private intervalId: any;

  constructor() { }

  /**
   * Clears the interval if it's still going, so i don't run unintended code by mistake.
   */
  public ngOnDestroy(): void {
    clearInterval(this.intervalId)
  }

  /**
   * Called when the user starts dragging the element. Starts the createParticle interval.
   * @param $event See https://material.angular.io/cdk/drag-drop/api#CdkDragStart
   */
  public dragStart($event: CdkDragStart): void {
    //Starts an interval that, if x and y is set, spawns a particle very 50 ms.
    //The interval id is saved, so it can be canceled later.
    this.intervalId = setInterval(() => {
      if (this.x != -1 && this.y != -1) {
        createParticle(this.x, this.y);
      }
    }, 50)
  }

  /**
   * Called when the user stops dragging the element. Clears the createParticle interval.
   * @param $event See https://material.angular.io/cdk/drag-drop/api#CdkDragEnd
   */
  public dragEnd($event: CdkDragEnd): void {
    //Clears the x and y, and clears the interval so it don't keep on going.
    this.y = -1;
    this.x = -1
    clearInterval(this.intervalId);
  }

  /**
   *  Triggered every time the element moves. Updates the x and y coordinates for the createParticle function.
   * @param $event See https://material.angular.io/cdk/drag-drop/api#CdkDragMove
   */
  public dragMove($event: CdkDragMove): void {
    this.x = $event.pointerPosition.x;
    this.y = $event.pointerPosition.y;
  }
}
