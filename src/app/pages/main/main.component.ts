import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { createParticle } from 'src/app/helper/particles.helper';
import { vibrate } from 'src/app/helper/sensor.helper';
import { GeoLocationService } from 'src/app/services/geolocation.service';
import { HighscoreService } from 'src/app/services/highscore.service';
import { PlayerService } from 'src/app/services/player.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
/**
 * Frontpage component. The main game is contained here.
 */
export class MainComponent implements OnInit, OnDestroy {

  public clicks: number = 0;
  public counter: number = 0;
  public cooldown: boolean = false;
  public name: string = '';
  public score: number = 0;
  public readonly time: number = 5000;
  private subscription: Subscription = Subscription.EMPTY;

  constructor(public playerService: PlayerService, public highscoreService: HighscoreService, public geoLocationService: GeoLocationService) { }

  public ngOnInit(): void {
    //By subscribing to the name, this code gets triggered if an update is made.
    this.subscription = this.playerService.name.subscribe(name => {
      this.name = name;
    })
  }
  //When this component is destroy (By going to another page), i make sure to unsubscribe to make sure i don't run unintended code.
  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /**
   * Activeted when the main button is pressed.
   * It first creates 10 particels. It then starts the timer, if it hasn't been started and then add one to the click count.
   * @param e The button click event triggered on the page
   */
  public buttonClick(e: MouseEvent): void {

    for (let i = 0; i < 10; i++) {
      // We pass the mouse coordinates to the createParticle() function.
      createParticle(e.clientX, e.clientY);
    }

    //If a game is under way, we don't want to start a new.
    if (this.counter === 0) {
      this.startTimer(this.name);
    }
    //Calls the vibrate helper function
    this.clicks++;
    vibrate(100);
  }
  /**
   * Whenever an update happens to the input field, this function is triggered.
   */
  public nameChange(): void {
    this.playerService.setName(this.name);
  }

  /**
   * The share score buttons triggers WEB SHARE API, which triggers the device to 'share' some content. In this case some text.
   */
  public shareScore(): void {
    //navigator contains a lot of API's. I use share to trigger the device to 'share' some content. It allows the user to pick where they want to share it to.
    //navigator also contains stuff like, location, permission and more.
    navigator.share({
      title: 'ClickGame Score',
      text: `Check out my score in ClickGame: ${this.clicks} clicks in ${this.time / 1000} seconds.`
    })
  }

  /**
   *
   * @param name Takes the player name, to be saved with the score. If the name changes doring the game, it doesn't affect the score.
   */
  private startTimer(name: string): void {
    this.clicks = 0;
    //startTime takes the current time, and stores it so it can be compared later
    var startTime = Date.now();
    let intervalId = setInterval(() => {
      //counter then contains how many milliseconds that is left.
      this.counter = this.time - (Date.now() - startTime);

      //When it hit 0, the game is done.
      //Because i use Date.now(), i might get below 0 by chance.
      if (this.counter <= 0) {
        this.counter = 0;
        //The score is when saved to a variable to be shown on the screen. The 'share' button is hidden if the score is 0.
        this.score = this.clicks;



        //The score is added to the highscore list. All validation is done in the service.
        //But before saving the score to the highscore list, it tries to get the gps location.
        //If an error happens (or no permisson is given), it just saves the score and the name.
        //In an earlier version, a locations name was also retrived from Google, but now that is done serverside.
        this.geoLocationService.getGPSLocation((geo) => {
          this.highscoreService.addScore({name: name, score: this.clicks, latitude: geo.coords.latitude, longitude: geo.coords.longitude, device_id: this.playerService.uuid()})
        }, (e) => {
          console.error(e);
          this.highscoreService.addScore({name: name, score: this.clicks, device_id: this.playerService.uuid()})
        });
        //clears the interval so the game stops.
        clearInterval(intervalId);
        //It then coolsdown, so a new game don't begin, with one to many clicks.
        this.cooldown = true;
        setTimeout(() => {
          this.cooldown = false;
        }, 5000);
      }
    }, 10)
  }
}
